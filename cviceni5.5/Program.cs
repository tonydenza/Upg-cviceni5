﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni5._5
{
    class Program
    {
        static int BinarniVyhledavaniPuvodni (int[] pole, int prvek)
        {
            int leva = 0, prava = pole.Length -1, stredni;

            while (leva <= prava)
            {
                stredni = leva + prava / 2;
                if (pole[stredni] == prvek)
                    return stredni;
                else if (pole[stredni] > prvek)
                    prava = stredni - 1;
                else
                    leva = stredni + 1;
            }

            return -1;
        }

        static int BinarniVyhledavani(int[] pole, int prvek)
        {
            int leva = pole.Length - 1, prava = 0, stredni;

            while (leva >= prava)
            {
                stredni = (leva + prava) / 2;

                while (prvek == pole[stredni])
                {
                    stredni--;
                    if (stredni == -1)
                    {
                        stredni++;
                        break;
                    }
                }

                if (pole[stredni] == prvek)
                    return stredni;
                else if (pole[stredni] > prvek)
                    leva = stredni - 1;
                else
                    prava = stredni + 1;
            }

            return -1;
        }

        static void Main(string[] args)
        {
            //int[] p1 = { 12, 9, 6, 6, 6, 5, 3, 1, 1 };
            int[] p1 = { 9, 9, 9, 9, 9, 9, 9, 9, 6, 6, 6, 6 };
            int index = BinarniVyhledavani(p1, 6);

            Console.Read();
        }
    }
}
