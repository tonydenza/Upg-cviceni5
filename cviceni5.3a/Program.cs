﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni5._3
{
    class Program
    {
        static int prirazeni, porovnani;

        static void Prirazeni(ref int x, int hodnota)
        {
            x = hodnota;
            prirazeni++;
        }

        static bool JeMensi(int cislo1, int cislo2)
        {
            porovnani++;
            if (cislo1 < cislo2)
                return true;
            return false;
        }

        static bool JeVetsi(int cislo1, int cislo2)
        {
            porovnani++;
            if (cislo1 > cislo2)
                return true;
            return false;
        }

        static void Prohozeni (ref int x, ref int y)
        {
            int tmp = 0;

            Prirazeni(ref tmp, x);
            Prirazeni(ref x, y);
            Prirazeni(ref y, tmp);
        }

        static void SelectionSort (int[] pole)
        {
            int max = 0;

            for (int i = 0; i < pole.Length; i++)
            {
                Prirazeni(ref max, i);
                for (int j = i; j < pole.Length; j++)
                    if (JeVetsi( pole[j], pole[max]))
                        Prirazeni(ref max, j);

                Prohozeni(ref pole[max], ref pole[i]);
            }
        }

        static void Main(string[] args)
        {
            int[] pole = { 5, 7, 1, 5, 6, 8, 9, 0, 1, 2 };
            SelectionSort(pole);
            Console.WriteLine("Selection sort - Prirazeni: {0}, Porovnani: {1}", prirazeni, porovnani);
            Console.ReadLine();
        }
    }
}
