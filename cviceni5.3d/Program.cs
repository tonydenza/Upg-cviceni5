﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni5._3
{
    class Program
    {
        static int prirazeni, porovnani;

        static void Prirazeni(ref int x, int hodnota)
        {
            x = hodnota;
            prirazeni++;
        }

        static void Prirazeni(ref bool x, bool hodnota)
        {
            x = hodnota;
            prirazeni++;
        }

        static bool JeMensi(int cislo1, int cislo2)
        {
            porovnani++;
            if (cislo1 < cislo2)
                return true;
            return false;
        }

        static bool JeVetsi(int cislo1, int cislo2)
        {
            porovnani++;
            if (cislo1 > cislo2)
                return true;
            return false;
        }

        static void Prohozeni(ref int x, ref int y)
        {
            int tmp = 0;

            Prirazeni(ref tmp, x);
            Prirazeni(ref x, y);
            Prirazeni(ref y, tmp);
        }

        static void CountSort(int[] pole)
        {
            int max = int.MinValue, k = 0;

            for (int i = 0; i < pole.Length; i++)
                if (JeVetsi(pole[i], max))
                    Prirazeni(ref max, pole[i]);
            
            int[] pomocnePole = new int[max + 1];
            int j = max;

            for (int i = 0; i < pole.Length; i++)
                Prirazeni(ref pomocnePole[pole[i]], pomocnePole[pole[i]] + 1);

            while (j >= 0)
            {
                if (JeVetsi(pomocnePole[j], 0))
                {
                    Prirazeni(ref pole[k], j);
                    Prirazeni(ref pomocnePole[j], pomocnePole[j] - 1);
                    Prirazeni(ref k, k + 1);
                }
                else
                    Prirazeni(ref j, j - 1);
            }
        }

        static void Main(string[] args)
        {
            int[] pole = { 5, 7, 1, 5, 6, 8, 9, 0, 1, 2 };
            CountSort(pole);
            Console.WriteLine("Count sort - Prirazeni: {0}, Porovnani: {1}", prirazeni, porovnani);
            Console.ReadLine();
        }
    }
}