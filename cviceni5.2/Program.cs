﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni5._2
{
    class Program
    {
        static int prirazeni, porovnani;

        static void Prirazeni (ref int x, int hodnota)
        {
            x = hodnota;
            prirazeni++;
        }

        static bool JeMensi (int cislo1, int cislo2)
        {
            porovnani++;
            if (cislo1 < cislo2)
                return true;
            return false;
        }

        static bool JeVetsi(int cislo1, int cislo2)
        {
            porovnani++;
            if (cislo1 > cislo2)
                return true;
            return false;
        }

        static void Main(string[] args)
        {
            int x = 5, y = 4, z = 3;
            bool je1 = JeMensi(y, z);
            bool je2 = JeMensi(x, z);
            bool je3 = JeVetsi(x, y);
            Prirazeni(ref x, y);
            Prirazeni(ref y, z);
            Console.WriteLine("Prirazeni: {0}, Porovnani: {1}", prirazeni, porovnani);
            Console.ReadLine();
        }
    }
}
