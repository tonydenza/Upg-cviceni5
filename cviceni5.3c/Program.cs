﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni5._3
{
    class Program
    {
        static int prirazeni, porovnani;

        static void Prirazeni(ref int x, int hodnota)
        {
            x = hodnota;
            prirazeni++;
        }

        static void Prirazeni(ref bool x, bool hodnota)
        {
            x = hodnota;
            prirazeni++;
        }

        static bool JeMensi(int cislo1, int cislo2)
        {
            porovnani++;
            if (cislo1 < cislo2)
                return true;
            return false;
        }

        static bool JeVetsi(int cislo1, int cislo2)
        {
            porovnani++;
            if (cislo1 > cislo2)
                return true;
            return false;
        }

        static void Prohozeni(ref int x, ref int y)
        {
            int tmp = 0;

            Prirazeni(ref tmp, x);
            Prirazeni(ref x, y);
            Prirazeni(ref y, tmp);
        }

        static void BubbleSort(int[] pole)
        {
            bool vymeneno = true;
            while (vymeneno)
            {
                Prirazeni(ref vymeneno, false);
                for (int i = 0; i < pole.Length - 1; i++)
                {
                    if (JeMensi(pole[i], pole[i+1]))
                    {
                        Prohozeni(ref pole[i], ref pole[i + 1]);
                        Prirazeni(ref vymeneno, true);
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            int[] pole = { 5, 7, 1, 5, 6, 8, 9, 0, 1, 2 };
            BubbleSort(pole);
            Console.WriteLine("Bubble sort - Prirazeni: {0}, Porovnani: {1}", prirazeni, porovnani);
            Console.ReadLine();
        }
    }
}
