﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace cviceni5._4
{
    class Program
    {
        static int prirazeni, porovnani;

        static void Prirazeni(ref int x, int hodnota)
        {
            x = hodnota;
            prirazeni++;
        }

        static void Prirazeni(ref bool x, bool hodnota)
        {
            x = hodnota;
            prirazeni++;
        }

        static bool JeMensi(int cislo1, int cislo2)
        {
            porovnani++;
            if (cislo1 < cislo2)
                return true;
            return false;
        }

        static bool JeVetsi(int cislo1, int cislo2)
        {
            porovnani++;
            if (cislo1 > cislo2)
                return true;
            return false;
        }

        static void Prohozeni(ref int x, ref int y)
        {
            int tmp = 0;

            Prirazeni(ref tmp, x);
            Prirazeni(ref x, y);
            Prirazeni(ref y, tmp);
        }

        static int[] VytvorPole (int prvky)
        {
            int[] pole = new int[prvky + 1];
            Random r = new Random();
            
            for (int i = 0; i < pole.Length; i++)
            {
                pole[i] = r.Next(0, prvky + 1);
            }

            return pole;
        }

        static void VypisPole (int[] pole)
        {
            for (int i = 0; i < pole.Length; i++)
                Console.Write("{0} ", pole[i]);
        }

        static int[] NakopirujPole (int[] pole)
        {
            int[] novePole = new int[pole.Length];

            for (int i = 0; i < pole.Length; i++)
                novePole[i] = pole[i];

            return novePole;
        }

        static void SelectionSort(int[] pole)
        {
            int max = 0;

            for (int i = 0; i < pole.Length; i++)
            {
                Prirazeni(ref max, i);
                for (int j = i; j < pole.Length; j++)
                    if (JeVetsi(pole[j], pole[max]))
                        Prirazeni(ref max, j);

                Prohozeni(ref pole[max], ref pole[i]);
            }
        }

        static void InsertionSort(int[] pole)
        {
            int min = 0, j = 0;
            for (int i = 1; i < pole.Length; i++)
            {
                Prirazeni(ref min, pole[i]);
                for (j = i; j > 0 && pole[j - 1] < min; j--)
                    Prirazeni(ref pole[j], pole[j - 1]);

                Prirazeni(ref pole[j], min);
            }
        }

        static void BubbleSort(int[] pole)
        {
            bool vymeneno = true;
            while (vymeneno)
            {
                Prirazeni(ref vymeneno, false);
                for (int i = 0; i < pole.Length - 1; i++)
                {
                    if (JeMensi(pole[i], pole[i + 1]))
                    {
                        Prohozeni(ref pole[i], ref pole[i + 1]);
                        Prirazeni(ref vymeneno, true);
                    }
                }
            }
        }

        static void CountSort(int[] pole)
        {
            int max = int.MinValue, k = 0;

            for (int i = 0; i < pole.Length; i++)
                if (JeVetsi(pole[i], max))
                    Prirazeni(ref max, pole[i]);

            int[] pomocnePole = new int[max + 1];
            int j = max;

            for (int i = 0; i < pole.Length; i++)
                Prirazeni(ref pomocnePole[pole[i]], pomocnePole[pole[i]] + 1);

            while (j >= 0)
            {
                if (JeVetsi(pomocnePole[j], 0))
                {
                    Prirazeni(ref pole[k], j);
                    Prirazeni(ref pomocnePole[j], pomocnePole[j] - 1);
                    Prirazeni(ref k, k + 1);
                }
                else
                    Prirazeni(ref j, j - 1);
            }
        }

        static void SetridVypis (int[] pole, int[] puvodniPole)
        {
            Stopwatch cas = new Stopwatch();
            long timeTicks, timeMs;

            cas.Start();
            SelectionSort(pole);
            cas.Stop();
            timeTicks = cas.ElapsedTicks;
            timeMs = cas.ElapsedMilliseconds;
            cas.Reset();
            Console.WriteLine("Selection Sort - porovnani: {0}, prirazeni: {1}", porovnani, prirazeni);
            Console.WriteLine("Selection Sort - {0} ticks, {1} ms", timeTicks, timeMs);
            porovnani = 0; prirazeni = 0;
            pole = NakopirujPole(puvodniPole);

            cas.Start();
            InsertionSort(pole);
            cas.Stop();
            timeTicks = cas.ElapsedTicks;
            timeMs = cas.ElapsedMilliseconds;
            cas.Reset();
            Console.WriteLine("Insertion Sort - porovnani: {0}, prirazeni: {1}", porovnani, prirazeni);
            Console.WriteLine("Insertion Sort - {0} ticks, {1} ms", timeTicks, timeMs);
            porovnani = 0; prirazeni = 0;
            pole = NakopirujPole(puvodniPole);

            cas.Start();
            BubbleSort(pole);
            cas.Stop();
            timeTicks = cas.ElapsedTicks;
            timeMs = cas.ElapsedMilliseconds;
            cas.Reset();
            Console.WriteLine("Bubble Sort - porovnani: {0}, prirazeni: {1}", porovnani, prirazeni);
            Console.WriteLine("Bubble Sort - {0} ticks, {1} ms", timeTicks, timeMs);
            porovnani = 0; prirazeni = 0;
            pole = NakopirujPole(puvodniPole);

            cas.Start();
            CountSort(pole);
            cas.Stop();
            timeTicks = cas.ElapsedTicks;
            timeMs = cas.ElapsedMilliseconds;
            cas.Reset();
            Console.WriteLine("Count Sort - porovnani: {0}, prirazeni: {1}", porovnani, prirazeni);
            Console.WriteLine("Count Sort - {0} ticks, {1} ms", timeTicks, timeMs);
            porovnani = 0; prirazeni = 0;
            pole = NakopirujPole(puvodniPole);

            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            // prvku 1000 
            int prvky = 1000;
            int[] pole = VytvorPole(prvky);
            int[] puvodniPole = NakopirujPole(pole);
            Console.WriteLine("Pocet prvku 1000");
            SetridVypis(pole, puvodniPole);

            //prvku 10000
            prvky = 10000;
            pole = VytvorPole(prvky);
            puvodniPole = NakopirujPole(pole);
            Console.WriteLine("Pocet prvku 10000");
            SetridVypis(pole, puvodniPole);

            //prvku 30000
            prvky = 30000;
            pole = VytvorPole(prvky);
            puvodniPole = NakopirujPole(pole);
            Console.WriteLine("Pocet prvku 30000");
            SetridVypis(pole, puvodniPole);

            Console.Read();
        }
    }
}
