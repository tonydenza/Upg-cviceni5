﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni5._1
{
    class Program
    {
        static bool JeSestupne (int[] pole)
        {
            for (int i = 0; i < pole.Length; i++)
            {
                if (i == pole.Length - 1)
                    break;

                if (pole[i] < pole[i + 1])
                    return false;
            }

            return true;
        }

        static void Main(string[] args)
        {
            int[] pole = { 1, 12, 9, 9, 8, 5, 2, 2, 1 };
            bool je = JeSestupne(pole);
            
Console.ReadLine();
        }
    }
}
